== Node Affinity

Mit Node Affinity hat man mehr Möglichkeiten, es bringt aber auch mehr Komplexität mit sich. +
Die gleiche Funktionalität wie beim Node Selector würde hier wie folgt aussehen:

["literal", subs="quotes"]
.pod-definition.yaml
----
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels: 
    type: frontend
spec:
  containers:
    - name: myapp-pod
      image: myapp-pod
  *affinity:*
    *nodeAffinity:*
      *requiredDuringSchedulingIgnoredDuringExecution:*
        *nodeSelectorTerms:*
          *- matchExpressions:*
            *- key: size*
              *operator: In*
              *values:*
                *- Large*
                *- Medium* //Hier ist die erste Erweiterung
----

In dem nächsten Beispiel wird das Label Small ausgeschlossen:
["literal", subs="quotes"]
.pod-definition.yaml
----
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels: 
    type: frontend
spec:
  containers:
    - name: myapp-pod
      image: myapp-pod
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
          - matchExpressions:
            *- key: size*
              *operator: NotIn*
              *values:*
                *- Small*
----

Da das Label Small im Kurs nicht vergeben wurde, würde somit auch folgendes funktionieren:
["literal", subs="quotes"]
.pod-definition.yaml
----
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels: 
    type: frontend
spec:
  containers:
    - name: myapp-pod
      image: myapp-pod
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
          - matchExpressions:
            *- key: size*
              *operator: Exists*
----


.Bereits vorhanden:
----
- requiredDuringSchedulingIgnoredDuringExecution
- preferredDuringSchedulingIgnoredDuringExecution
----

.Geplant:
----
- requiredDuringSchedulingRequiredDuringExecution
----