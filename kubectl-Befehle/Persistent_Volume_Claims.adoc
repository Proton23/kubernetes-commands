== Persistent Volume Claims

Ein _PVC_ ist das Verbindungsstück zwischen einem _PV_ und einem POD.

.pv-definition.yaml
["literal", subs="quotes"]
----
apiVersion: *v1*
kind: *PersistentVolumeClaim*
metadata:
  name: myClaim
spec: 
 *accessModes:*
   *- ReadWriteOnce*
 *resources:*
   *requests:*
     *storage: 500Mi* 
 *persistentVolumeReclaimPolicy: Retain*   
----

Beim Attribut `persistentVolumeReclaimPolicy` ist *Retain* der Standardwert, dabei wird das _PersistentVolume_ erhalten. Bei der Option *Delete* wird das _PV_ gelöscht. Bei der Option *Recyle* wird der Speicherplatz gelöscht und das _PV_ wird erneut verwendet.



