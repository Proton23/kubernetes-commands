== Allgemein

=== Erzeugen von Kubernetes-Objekten

==== Imperativ

Hierbei werden Objekte erzeugt durch den Befehl der ausgeführt wird
----
node:~$ kubectl create configmap
----

==== Deklarativ

Die Objekte werden über eine Definitionsdatei erzeugt
----
node:~$ kubectl create -f <definition-file>.yaml
----

=== Aufbau von Definitiondateien in Kubernetes
Alle Definitionsdateien sind wie folgt aufgebaut:

----
apiVersion: 
kind: 
metadata:
  [...]
spec:
  [...]
----

=== Typen von Kind

[cols="1,1"]
|===
|Kind |Version 

|Pod
|v1

|Service
|v1

|ReplicaSet
|apps/v1

|Deployment
|apps/v1

|ResourceQuota
|v1

|ConfigMap
|v1

|Job
|batch/v1

|CronJob
|batch/v1

|Ingress
|networking.k8s.io/v1

|NetworkPolicy
|networking.k8s.io/v1

|PersistentVolume
|v1

|PersistentVolumeClaim
|v1
|===

=== Liste von Resourcetypen
[cols="1,1"]
|===
|Name |Kurzform 

|configmaps
|cm

|cronjobs
|cj

|deployments
|deploy

|ingresses
|ing

|jobs
|

|namespaces
|ns

|networkpolicies
|netpol

|nodes
|no

|persistentvolumes
|pv

|persistentvolumeclaims
|pvc

|pods
|po

|replicasets
|rs

|replicationcontrollers
|rc

|resourcequotas
|quota

|roles
|

|secrets
|

|serviceaccounts
|sa

|statefulsets
|sts

|storageclasses
|sc

|volumeattachments
|
|===

=== Bearbeiten von Definitionsdateien
----
node:~$ kubectl replace -f <definition-file>.yaml
----

=== Optionen anzeigen lassen

Alle Optionen anzeigen lassen:

----
node:~$ kubectl explain <type> --recursiv | less
----

=== Informationen 

von einem Objekt

----
node:~$ kubectl get pod <pod-name>
----

von mehreren Objekten

----
node:~$ kubectl get pods,svc
----

[caption="Hinweis"]
NOTE: Kein Leerzeichen zwischen den Objekten

von allen Objekten

----
node:~$ kubectl get all
----

=== Objekte entfernen

----
node:~$ kubectl delete <type> <object-name>
----


=== Aufruf testen

.Testen ob der Aufruf erfolgreich wäre
["literal", subs="quotes"]
----
node:~$ kubectl run redis --image=redis *--dry-run=client*
----

Mit solch einem Testaufruf lässt sich auch eine Definitionsdatei erstellen.

.Einfaches erzeugen einer Definitionsdatei
["literal", subs="quotes"]
----
node:~$ kubectl run redis --image=redis --dry-run=client *-o yaml > &lt;dateiname&gt;.yaml*
----
