==  

[caption="Hinweis"]
NOTE: Als _Ingress_ wird Datenverkehr bezeichnet der hereinkommt. +
Datenverkehr der herausgeht wird als _Egress_ bezeichnet.

[caption="Hinweis"]
NOTE: Erlaubt man _Ingress_ Datenverkehr, ist der Datenverkehr zurück automatisch erlaubt und benötigt keine separate Regel.

=== Ingress

.network-policy.yaml
["literal", subs="quotes"]
----
apiVersion: *networking.k8s.io/v1*
kind: *NetworkPolicy*
metadata:
  name: db-policy
spec:
  *podSelector:*
    *matchLabels:*
      *role: db*
  *policyTypes:*
    *- Ingress*
  *ingress:*
    *- from:*
      *- podSelector:*
          *matchLabels:*
            *name: api-pod*
        *namespaceSelector:*
          *matchLabels:*
            *name: prod*
      *- ipBlock:*
          *cidr: 192.168.5.10/32*  
      *ports:* 
        *- protocol: TCP*
          *port: 3306*
----

Hier bei ist es wichtiig anzumerken, dass `podSelector` und `namespaceSelector` eine Regel darstellen. Es ist werden nur PODs zugelassen, die beide Bedingungen erfüllen. 


=== Egress

.network-policy.yaml
["literal", subs="quotes"]
----
apiVersion: *networking.k8s.io/v1*
kind: *NetworkPolicy*
metadata:
  name: db-policy
spec:
  podSelector:
    matchLabels:
      role: db
  policyTypes:
    - Ingress
   *- Egress*
  ingress:
    - from:
      - podSelector:
          matchLabels:
            name: api-pod 
      ports: 
        - protocol: TCP
          port: 3306
 *egress:*
   *- to:*
     *- ipBlock:*
         *cidr: 192.168.5.10/32*
     *ports:*    
       *- protocol: TCP*
         *port: 80*
----