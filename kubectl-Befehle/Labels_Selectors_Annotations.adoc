== Labels, Selectors and Annotation

=== Labels

.Label imperativ setzen
["literal", subs="quotes"]
----
node:~$ kubectl run redis --image=redis *-l app=app1*
----

.pod-defintion
["literal", subs="quotes"]
----
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  *labels:*
    *app: app1*
    *type: frontend*
    *function: Base* 
spec: 
  containers: 
    - name: nginx-container
      image: nginx
----

=== Selector

.Selector
["literal", subs="quotes"]
----
node:~$ kubectl get pod *--selector app=app1*
----

.Annotations
["literal", subs="quotes"]
----
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: app1
    type: frontend
  *annotations:*
    *buildversion: 1.34*
spec: 
  containers: 
    - name: nginx-container
      image: nginx
----