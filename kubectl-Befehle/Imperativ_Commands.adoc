== Imperative Commands

Es ist wichtig Objekte auch direkt erstellen zu können und nicht über den Umweg einer Definitionsdatei gehen zu müssen.

=== Hilfe

.Hilfe für den Befehl `create`
["literal", subs="quotes"]
----
node:~$ kubectl create -h;
node:~$ kubectl create --help;
----

=== Available Commands
[cols="1,1"]
|===
|Objekt |Was geschieht? 

|clusterrole         |Create a ClusterRole.
|clusterrolebinding  |Create a ClusterRoleBinding for a particular ClusterRole
|configmap           |Create a configmap from a local file, directory or literal value
|cronjob             |Create a cronjob with the specified name.
|deployment          |Create a deployment with the specified name.
|ingress             |Create an ingress with the specified name.
|job                 |Create a job with the specified name.
|namespace           |Create a namespace with the specified name
|poddisruptionbudget |Create a pod disruption budget with the specified name.
|priorityclass       |Create a priorityclass with the specified name.
|quota               |Create a quota with the specified name.
||role               |Create a role with single rule.
|rolebinding         |Create a RoleBinding for a particular Role or ClusterRole
|secret              |Create a secret using specified subcommand
|service             |Create a service using specified subcommand.
|serviceaccount      |Create a service account with the specified name

|===

=== Tolerations erstellen

["literal", subs="quotes"]
.pod-definition.yaml
----
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels: 
    type: frontend
spec:
  containers:
    - name: myapp-pod
      image: myapp-pod
  *tolerations:*
    *- key: "app"*
      *operator: "Equal"*
      *value: "blue"*
      *effect: "NoSchedule"*
----

[caption="Hinweis"]
NOTE: Alle Werte müssen in Anführungszeichen stehen