== Namespaces

=== Erstellen eines Namespace

["literal", subs="quotes"]
----
apiVersion: v1
kind: *Namespace*
metadata:
  name: dev
----

Oder direkt über ein Kommando
----
node:~$ kubectl create namespace dev 
----

=== Aufbau der Namespace Namen
["literal", subs="quotes"]
----
db-service.dev.svc.*cluster.local* //default domain-name 
db-service.dev.*svc*.cluster.local //subdomain for service 
db-service.*dev*.svc.cluster.local //namespace 
*db-service*.dev.svc.cluster.local //service name 
----

=== Anzeige von Namespaces
["literal", subs="quotes"]
----
node:~$ kubectl get *namespace*
----

Kurzform
["literal", subs="quotes"]
----
node:~$ kubectl get *ns*
----

=== Anzeige von Objekten in Namespace 

Pods anzeigen lassen aus anderem Namespace
["literal", subs="quotes"]
----
node:~$ kubectl get pod *--namespace=kube-system*
----

Kurzform
["literal", subs="quotes"]
----
node:~$ kubectl get pod *-n kube-system*
----

Pods aus allen Namespaces anzeigen
["literal", subs="quotes"]
----
node:~$ kubectl get pod *--all-namespaces*
----

=== Option Namespace

Die Namespace-Option kann fast bei allen Befehlen angewendet werden
["literal", subs="quotes"]
----
node:~$ kubectl create -f pod-definition.yaml *--namespace=dev*
----

=== Namespace dauerhaft wechseln

----
node:~$ kubectl config set-context $(kubectl config current-context) --namespace=dev
----

=== Objekte in Namespace erzeugen
Man kann bestimmen in welchem Namespace ein Objekt erzeugt wird, indem man diesen unter Metadata einträgt

["literal", subs="quotes"]
----
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  *namespace: dev*
  labels: 
    beliebiger: wert
    type: frontend
spec: 
  containers: 
    - name: nginx-container
      image: nginx
----